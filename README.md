# Paginated HTML Documents

This is my solution to paginating HTML documents in such a way that pages on screen and on paper are rendered in an
identical fashion.

You simply need to include the CSS file
[`paginate.css`](https://gitlab.com/voidyourwarranty/paginate/blob/master/paginate.css) and add `class="a4-portrait"` to
your `<body>` tag. Then the contents of each `<div class="page">` are printed on a new page. The sample CSS file
provides the paper sizes `a4-portrait`, `a4-landscape`, `letter-portrait` and `letter-landscape`. You can easily add
further paper sizes when you take a look at the CSS file and replace the `width:` and `height:` values.

The example HTML file demonstrates this for A4 paper in portrait mode. You can easily notice from the screen rendering
when a printed page would overflow.

## License

[GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
